package no.ntnu.imt3281.eksamen.exchange_rates;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Used to store observations/exchange rates for a single currency.
 * Only the actual exchange rates are stored with the three letter acronym and a description for this currency.
 * Note, also the multiplier for this currency is stored, used for calculating currency exchange.
 */
public class Rates {
    String currency;
    String description;
    ArrayList<Double> observations = new ArrayList<>();
    private int multiplier;

    /**
     * create a new rates object for the currency with the given acronym.
     *
     * @param s the acronym for this currency
     */
    public Rates(String s) {
        this.currency = s;
    }

    /**
     * Get the number of observations/currency exchange rates stored for this currency.
     *
     * @return the number of observations.
     */
    public int nrOfObservations() {
        return observations.size();
    }

    /**
     * Return a single observation/currency exchange rate.
     *
     * @param idx the index of the observation to return.
     * @return the actual currency exchange rate for this observation.
     */
    public double getObservation(int idx) {
        return observations.get(idx);
    }

    /**
     * @see Object#toString()
     * @return the currency acronym, description and observations for this currency.
     */
    @Override
    public String toString() {
        return currency+" "+description+" "+observations;
    }

    /**
     * Set the description for this currency.
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the observations for this currency from a JSON object and stores them in an arraylist.
     * @param observations a JSON object containing the observations for this currency.
     */
    public void setObservations(JSONObject observations) {
        Iterator it = observations.keys();
        while (it.hasNext()) {
            this.observations.add(observations.getJSONArray(it.next().toString()).getDouble(0));
        }
    }

    /**
     * Sets the multiplier for this currency, used for currency exchange.
     *
     * @param multiplier the multiplier for this currency (1 or 100)
     */
    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    /**
     * Take an amount in NOK and convert it to this currency.
     *
     * @param amount the amount of money in NOK to exchange.
     * @return the amount of money you will receive in this currency.
     */
    public double exchangeTo(double amount) {
        return amount/observations.get(observations.size()-1)*multiplier;
    }

    /**
     * If given this amount of money in this currency, what amount of money will that be in NOK.
     *
     * @param amount the amount of money in this currency to echange.
     * @return the amount of money in NOK that the above amount of money represents.
     */
    public double exchangeFrom(double amount) {
        return amount*observations.get(observations.size()-1)/multiplier;
    }

    /**
     * Get all observations as an array of Double.
     *
     * @return an array containing all observations for this currency.
     */
    public Double[] getRates() {
        return observations.toArray(new Double[observations.size()]);
    }
}
