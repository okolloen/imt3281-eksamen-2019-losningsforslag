package no.ntnu.imt3281.eksamen.exchange_rates;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.net.MalformedURLException;
import java.net.URL;

public class ExchangeCalculatorController {
    @FXML private TextField nok;
    @FXML private ComboBox<String> currencySelector;
    @FXML private TextField result;
    private ObservableList<String> currencies = FXCollections.observableArrayList();

    private ExchangeRates rates;

    /**
     * Read data from Norges bank and fill an observable list with information about currencies that can be selected.
     * The content of this observable list will be shown in a combo box (drop down box).
     */
    public ExchangeCalculatorController() {
        try {
            rates = new ExchangeRates(new URL("https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=2019-10-01&endPeriod=2019-10-04&format=sdmx-json&locale=no"));
            for (String currency : rates.getCurrencies()) {
                currencies.add(currency+" ("+rates.getCurrencyDescription(currency)+")");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            currencies.add("Unable to get currency data");
        }
    }

    /**
     * Add listeners to input text field and drop down box.
     */
    @FXML
    void initialize() {
        currencySelector.setItems(currencies);  // Fill drop down box with currencies
        nok.textProperty().addListener((ObservableValue<? extends String> observableValue, String oldValue, String newValue) -> {
            if (newValue.equals("")) {  // Do nothing if no value is entered

            } else {
                try {
                    double value = Double.parseDouble(newValue);    // Try to convert content of text element to double value
                    if (currencySelector.getSelectionModel().getSelectedItem() == null) {   // If no currency selected
                        result.setText("Select a currency to convert to above");
                    } else {    // Currency is selected and a value has been entered, show new value
                        result.setText(String.format("%.2f", rates.exchangeTo(value, currencySelector.getSelectionModel().getSelectedItem().substring(0, 3))));
                    }
                } catch (NumberFormatException e) { // Non numeric value entered, revert to previous content
                    nok.setText(oldValue);
                }
            }
        });
        // When a new currency is selected, do a new currency conversion
        currencySelector.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observableValue, String oldValue, String newValue) -> {
            result.setText(String.format("%.2f", rates.exchangeTo(Double.parseDouble(nok.getText()), newValue.substring(0, 3))));
        });
    }
}
