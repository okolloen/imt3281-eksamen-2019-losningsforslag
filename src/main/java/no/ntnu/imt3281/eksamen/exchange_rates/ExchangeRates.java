package no.ntnu.imt3281.eksamen.exchange_rates;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Used to get currency exchange rates from Norges Bank and to simplify using this for currency exchange calculations
 * and getting all observation dates and rates for a given currency.
 */
public class ExchangeRates {
    HashMap<String, Rates> exchangeRates = new HashMap<String, Rates>();
    ArrayList<String> currencies = new ArrayList<>();
    ArrayList<Date> observationDates = new ArrayList<Date>();
    Logger LOGGER = Logger.getLogger(ExchangeRates.class.getPackageName());

    /**
     * Create a new ExchangeRates objects using data from the given JSON string.
     * Primarily used for testing since this allows an object to be initialized with known data.
     *
     * @param json the data to be used for initializing the object. Should follow the same format as the resources described here : https://www.norges-bank.no/tema/Statistikk/Valutakurser/?tab=api
     */
    public ExchangeRates(String json) {
        initFromJSON(json);
    }

    /**
     * Create a new ExchangeRates object using data from the given url.
     *
     * @param url this url should point to a resource with exchange rates in  JSON format.
     *            More specific, it should point to one of the resources described at : https://www.norges-bank.no/tema/Statistikk/Valutakurser/?tab=api
     */
    public ExchangeRates(URL url) {
        try {
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String input;
                StringBuffer sb = new StringBuffer();
                while ((input = br.readLine()) != null) {
                    sb.append(input);
                }
                initFromJSON(sb.toString());
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Unable to read from URL "+url, e);
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Unable to connect to URL "+url, e);
        }
    }

    /**
     * Parse the JSON data from Norges Bank and fill inn the internal data structure of this class.
     *
     * @param json the JSON data as read from Norges Bank.
     */
    private void initFromJSON(String json) {
        JSONObject rates = new JSONObject(json);
        JSONObject structure = rates.getJSONObject("structure");
        JSONArray currencies = structure.getJSONObject("dimensions").getJSONArray("series").getJSONObject(1).getJSONArray("values");
        for (Object o : currencies) {               // Get all currencies registered in the data structure
            JSONObject currency = (JSONObject)o;
            this.currencies.add(currency.getString("id"));
        }
        JSONArray observationDates = structure.getJSONObject("dimensions").getJSONArray("observation").getJSONObject(0).getJSONArray("values");
        for (Object o : observationDates) {         // Get all observation dates from the data structure
            try {
                Date d = new SimpleDateFormat("yyyy-MM-dd").parse (((JSONObject)o).getString("name"));
                this.observationDates.add(d);
            } catch (ParseException e) {
                LOGGER.log(Level.INFO, "Unable to parse date: "+ ((JSONObject)o).getString("name"), e);
            }
        }
        JSONArray attributes = structure.getJSONObject("attributes").getJSONArray("series");

        JSONObject observations = rates.getJSONArray("dataSets").getJSONObject(0).getJSONObject("series");
        Iterator it = observations.keys();
        while (it.hasNext()) {                      // Get all actual observations from the JSON data
            String key = it.next().toString();
            int currencyId = Integer.parseInt(key.split(":")[1]);
            Rates r = new Rates(this.currencies.get(currencyId));
            r.setDescription (currencies.getJSONObject(currencyId).getString("name"));
            r.setObservations(observations.getJSONObject(key).getJSONObject("observations"));
            r.setMultiplier(observations.getJSONObject(key).getJSONArray("attributes").getInt(2)==0?1:100);
            exchangeRates.put(r.currency, r);
        }
    }

    /**
     * Checks to see if a given currency is registered.
     *
     * @param currency the short name of the currency to check for
     * @return true if the currency exists, otherwise false
     */
    public boolean currencyExists(String currency) {
        return currencies.contains(currency);
    }

    /**
     * Used to get an array with all the registered currencies that one can get exchange rates for.
     *
     * @return an array with the short name for all the registered currencies.
     */
    public String[] getCurrencies() {
        return currencies.toArray(new String[currencies.size()]);
    }

    /**
     * Get an array of double with registered exchange rates for a given currency.
     * Use the {@see getDates} method to get the corresponding dates.
     *
     * @param currency the currency to get exchange rates for.
     * @return an array of Double with exchange rates.
     */
    public Double[] getExchangeRates(String currency) {
        return exchangeRates.get(currency).getRates();
    }

    /**
     * Get dates where exchange rates are registered.
     *
     * @return an array of Date objects where exchange rates are registered.
     */
    public Date[] getDates() {
        return observationDates.toArray(new Date[observationDates.size()]);
    }

    /**
     * Exchange the given amount to the given currency.
     *
     * @param amount the amount of money to exchange (in NOK)
     * @param currency the currency to exchange the money into
     * @return the amount of money in the new currency
     */
    public double exchangeTo(double amount, String currency) {
        return exchangeRates.get(currency).exchangeTo(amount);
    }

    /**
     * Exchange the given amount from the given currency into NOK.
     *
     * @param amount the amount of money in foreign currency
     * @param currency the currency to exchange from
     * @return the amount of money in NOK
     */
    public double exchangeFrom(double amount, String currency) {
        return exchangeRates.get(currency).exchangeFrom(amount);
    }

    /**
     * Get the long name of a given currency.
     *
     * @param currency the currency ("SEK", "USD" etc) to get the long name for
     * @return the long name for the given currency
     */
    public String getCurrencyDescription(String currency) {
        return exchangeRates.get(currency).description;
    }
}