package no.ntnu.imt3281.eksamen.exchange_rates;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExchangeRatesController {

    @FXML private ComboBox<String> currencySelector;
    @FXML private VBox box;

    private ObservableList<String> currencies = FXCollections.observableArrayList();
    private ExchangeRates rates;
    private boolean graphInitialized = false;
    private SimpleDateFormat dformat = new SimpleDateFormat("dd/MM/YYYY");

    /**
     * Load exchange rates from Norges Bank and fill observable list with selectable currencies.
     */
    public ExchangeRatesController() {
        try {
                rates = new ExchangeRates(new URL("https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=2019-09-08&endPeriod=2019-10-08&format=sdmx-json&locale=no"));
            for (String currency : rates.getCurrencies()) {
                currencies.add(currency+" ("+rates.getCurrencyDescription(currency)+")");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            currencies.add("Unable to get currency data");
        }
    }

    /**
     * Create a Line chart and add it to the layout. Set custom tick label formatter for the x axis
     * to show dates correctly.
     */
    @FXML
    void initialize() {
        currencySelector.setItems(currencies);
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Date");
        xAxis.setMinorTickVisible(false);
        yAxis.setMinorTickVisible(false);
        xAxis.setTickLabelFormatter(new StringConverter<Number>() {
            @Override
            public String toString(Number number) {
                if (!graphInitialized||number.intValue()>=rates.getDates().length) {
                    return "";
                } else {
                    if (number.intValue() == number.floatValue()) {
                        return dformat.format(rates.getDates()[number.intValue()]);
                    } else {
                        return "";
                    }
                }
            }

            @Override
            public Number fromString(String s) {
                return null;
            }
        });
        final LineChart<Number, Number> chart = new LineChart<Number, Number>(xAxis, yAxis);
        chart.setCreateSymbols(false);
        box.getChildren().add(chart);
        // Redraw the graph when a new currency is selected
        currencySelector.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observableValue, String oldValue, String newValue) -> {
            XYChart.Series series = new XYChart.Series();
            series.setName("Exchange rates for "+newValue);
            String currency = newValue.substring(0, 3);
            Date[] dates = rates.getDates();
            Double[] observations = rates.getExchangeRates(currency);
            for (int i=0; i<dates.length; i++) {
                series.getData().add(new XYChart.Data<Integer, Double>(i, observations[i]));
            }
            chart.getData().clear();
            chart.getData().add(series);
            System.out.println(dates[0].getTime());
            ((NumberAxis)chart.getXAxis()).setLowerBound(dates[0].getTime());
            ((NumberAxis)chart.getXAxis()).setUpperBound(dates[dates.length-1].getTime());
            graphInitialized = true;
        });
    }
}

