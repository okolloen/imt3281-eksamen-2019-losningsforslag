package no.ntnu.imt3281.eksamen.mmp.server;

import org.json.JSONObject;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
    import java.util.concurrent.LinkedBlockingQueue;

/**
 * A fully functioning server for a MMP network. Clients can connect to the server and subscribe to channels/topics
 * and receive messages sent to that channel. Clients can also publish messages to channels (both subscribed and
 * unsubscribed.)
 */
public class MMPServer {
    private static final int SERVERPORT = 4567;
    private final boolean requireUnamePwd;
    private boolean stopping=false;
    private LinkedList<Client> clients= new LinkedList<>();
    private static final String UNAME = "very";
    private static final String PWD = "secret";
    private LinkedBlockingQueue<Client> clientsToRemove = new LinkedBlockingQueue<>();
    private ConcurrentHashMap<String, LinkedList<Client>> channels = new ConcurrentHashMap<>();


    /**
     * Start the MMPServer, if the passed parameter is true the server is to require username/password
     * to accept connections otherwise username/password is not require.
     *
     * @param requireUnamePwd true if the server should require username/password, false means no username/password is required.
     */
    public MMPServer(boolean requireUnamePwd) {
        this.requireUnamePwd = requireUnamePwd;
        startServerThread();
        startReaderThread();
    }

    /**
     * Starts the thread that is responsible for reading messages from all the clients and also send messages
     * to clients when requested.
     */
    private void startReaderThread() {
        new Thread() {
            public void run() {
                while (!stopping) {
                    clients.stream().parallel().forEach(c -> {  // Loop over all connected clients
                        try {
                            String tmp = c.read();
                            if (tmp != null) {                  // Message received from client
                                JSONObject json = new JSONObject(tmp);
                                if (json.getString("action").equals("disconnect")) {
                                    disconnect(c);
                                } else if (json.getString("action").equals("subscribe")) {
                                    subscribe(c, json.getString("topic"));
                                } else if (json.getString("action").equals("publish")) {
                                    publish(c, json.getString("topic"), json.getString("subject"));
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                    // Oppgave 2g
                    clientsToRemove.stream().forEach(c -> {     // Loop over all clients to remove
                        c.channels.stream().parallel().forEach(channel -> { // Loop over all channels for that client
                            channels.get(channel).remove(c);                // Remove client from channel
                        });
                        clients.remove(c);                      // Remove client from list of clients
                    });
                    clientsToRemove.clear();                    // Clear list of clients to remove
                    // Oppgave 2g
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    /**
     * The given client want to publish a message to the given topic/channel with the given message/subject.
     * Sends the message to all clients subscribed to that topic with the message.
     * When done sending, sends a message to the client sending the message with information about
     * the number of clients who received the message.
     *
     * @param c the client sending the message
     * @param topic the channel to send the message to
     * @param subject the content of the message
     * @throws IOException if an IO exception occurred while sending the confirmation message.
     */
    private void publish(Client c, String topic, String subject) throws IOException {
        JSONObject res = new JSONObject();
        JSONObject msg = new JSONObject();
        msg.put("topic", topic);
        msg.put("subject", subject);
        res.put("message", msg);
        String resString = res.toString();
        if (channels.containsKey(topic)) {  // Such a channel/topic exists
            LinkedList<Client> clients = channels.get(topic);
            synchronized (clients) {
                clients.stream().parallel().forEach(client -> { // Loop over all clients
                    try {
                        client.write(resString);
                    } catch (IOException e) {
                        clientsToRemove.add(client);    // Oppgave 2g
                    }
                });
            }
            res = new JSONObject();
            res.put("published", topic);
            res.put("recipients", clients.size());
            c.write(res.toString());                // Send message back to client who sent the message
        } else {        // No such channel/topic
            res = new JSONObject();
            res.put("published", topic);
            res.put("recipients", 0);
            c.write(res.toString());                // Send message back to client
        }
    }

    /**
     * Add a client to the given topic/channel.
     * Send and acknowledgement to the client when done.
     *
     * @param c the client to add
     * @param topic the topic to add the client to
     * @throws IOException
     */
    private void subscribe(Client c, String topic) throws IOException {
        synchronized (channels) {
            if (channels.containsKey(topic)) {  // The channel/topic exists
                LinkedList<Client> clients = channels.get(topic);
                synchronized (clients) {
                    clients.add(c);
                }
            } else {                            // The channel/topic does not exist
                LinkedList<Client> clients = new LinkedList<>();
                clients.add(c);
                channels.put(topic, clients);
            }
        }
        JSONObject res = new JSONObject();
        res.put("subscribed", topic);
        c.write(res.toString());
    }

    /**
     * Sends acknowledgement message to client and adds client to queue of clients to remove.
     * @param c the client requesting disconnect
     */
    private void disconnect(Client c) throws IOException {
        JSONObject res = new JSONObject();
        res.put("disconnected", "OK");
        c.write(res.toString());
        clientsToRemove.add(c);     // Oppgave 2g
    }

    /**
     * Starting the thread that listens on the server socket and creates a new client object and calls the add.
     */
    private void startServerThread() {
        new Thread() {
            public void run () {
                try {
                    ServerSocket server = new ServerSocket(SERVERPORT);
                    while (!stopping) {
                        Socket s = server.accept();
                        BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
                        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
                        Client c = new Client(bw, br);
                        addClient(c);
                    }
                    server.close();
                } catch (java.net.BindException e) {
                    // Probably caused by multiple servers starting
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * Reads the connect message from the client and checks the protocol and version number. If the protocol and
     * version information is correct the username/password is checked (if the server is started in that mode).
     * If all information is correct the client is added to the list of clients, and a connected: OK message
     * is sent to the client.
     * If any information is incorrect, a message informing the client about the error is sent back and the
     * client is NOT added to the list of client.
     *
     * @param client a Client object which holds the reader/writer for communicating with the client.
     */
    public void addClient(Client client) {
        try {
            String tmp = client.read();
            JSONObject json = new JSONObject(tmp);
            JSONObject res = new JSONObject();
            if (!json.getString("protocol").equals("MMP")) {
                res.put("connected", "FAIL");
                res.put("reason", "Bad protocol/protocol version");
            } else if (json.getDouble("version")!=0.1) {
                res.put("connected", "FAIL");
                res.put("reason", "Bad protocol/protocol version");
            }
            // Oppgave 2c
            if (requireUnamePwd) {
                if (!json.has("user")||
                        !(json.getJSONObject("user").getString("uname").equals(UNAME)&&
                                json.getJSONObject("user").getString("pwd").equals(PWD))) {
                    res.put("connected", "FAIL");
                    res.put("reason", "Bad username/password");
                }
            }
            // Slutt på oppgave 2c
            if (!res.has("connected")) { // No errors, we are good to connect
                res.put("connected", "OK");
                clients.add(client);
            }
            client.write(res.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * A class used to represent a single client on the server. Contains the reader and writer used to communicate
     * with the client as well as a list of channels/topics that this client is subscribed to.
     */
    public static class Client {
        BufferedWriter bw = null;
        BufferedReader br = null;
        LinkedList<String> channels = new LinkedList<>();

        /**
         * Creates a new Client object, takes the reader and writer used to communicate with the client
         * as parameters and stores them in the object.
         *
         * @param bw the writer used to send messages to the client
         * @param br the reader used to receive message from the client
         */
        public Client (BufferedWriter bw, BufferedReader br) {
            this.br = br;
            this.bw = bw;
        }

        /**
         * Convenience method used to read messages from the client. Checks the reader if data is available and
         * if so return that data, if no data is available return null.
         *
         * @return any available data or null if no data is available.
         * @throws IOException if there are problems reading from the client.
         */
        public String read () throws IOException {
            if (br.ready()) {
                return br.readLine();
            }
            return null;
        }

        /**
         * Convenience method used to send data to the client, makes sure all data is followed by a newline character and
         * that the buffer is flushed when data has been sent.
         *
         * @param msg the message to send to the client.
         * @throws IOException if there are problems sending data to the client.
         */
        public void write(String msg) throws IOException {
            bw.write(msg);
            bw.newLine();
            bw.flush();
        }
    }
}
