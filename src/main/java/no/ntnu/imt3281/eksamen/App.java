package no.ntnu.imt3281.eksamen;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        VBox box  = new VBox();     // Use a vbox to place content from two fxml files below each other
        box.setSpacing(5);
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("exchangeCalculator.fxml"));
        box.getChildren().add(fxmlLoader.load());   // Add content from first fxml file
        fxmlLoader = new FXMLLoader(App.class.getResource("exchangeRates.fxml"));
        box.getChildren().add(fxmlLoader.load());   // Add content from second fxml file
        scene = new Scene(box);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}